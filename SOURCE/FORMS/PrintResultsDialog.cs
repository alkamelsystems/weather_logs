﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace WeatherLogs.FORMS
{
    public partial class PrintResultsDialog : Form
    {
        int copies;
        string printer;
        WLParameters appParams = null;

        public PrintResultsDialog(WLParameters appParams)
        {
            InitializeComponent();

            this.appParams = appParams;
            this.copies = 0;
            this.printer = "";
            loadPrinters();
        }

        private void loadPrinters() {

            System.Drawing.Printing.PrinterSettings printerSettings =  new System.Drawing.Printing.PrinterSettings();

            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                comboBox.Items.Add(printer);	 
            }

            comboBox.SelectedItem = printerSettings.PrinterName;

        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            try
            {
                copies = int.Parse(numericUpDown.Value.ToString());
                printer = comboBox.SelectedItem.ToString();
                this.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Error printing results", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public int getCopies()
        {
            return copies;
        }

        public string getPrinter()
        {
            return printer;
        }
    }
}
