﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Alkamel.Utils.Db;
using Alkamel.Utils.Images;

namespace WeatherLogs.FORMS
{
    public partial class ApplicationSetupDialog : Form
    {
        private WLParameters appParams;
        private DBConnection dbConn;

        public ApplicationSetupDialog(WLParameters appParams, DBConnection dbConn)
        {
            InitializeComponent();

            this.dbConn = dbConn;
            this.DialogResult = DialogResult.Cancel;
            this.appParams = appParams;

            txtDbServer.Text = appParams.DBServer;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                appParams.DBServer = txtDbServer.Text;

                WeatherLogs.WLParameters.SetAppParameters(Application.StartupPath, appParams);

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Save Settings", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
