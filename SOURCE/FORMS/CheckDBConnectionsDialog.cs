﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeatherLogs;

namespace ReportDistributor.FORMS
{
    public partial class CheckDBConnectionsDialog : Form
    {
        WLDataAccess appDataAccess = null;
        WLParameters appParams = null;

        public CheckDBConnectionsDialog(WLParameters appParams, WLDataAccess appDataAccess, string message)
        {
            InitializeComponent();
            this.DialogResult = DialogResult.Cancel;
            textBox.Text = appParams.DBServer;
            this.appDataAccess = appDataAccess;
            this.appParams = appParams;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            string m;

            buttonClose.Enabled = false;
            buttonSave.Enabled = false;

            Application.DoEvents();

            appParams.DBServer = textBox.Text.Trim();
            if (appDataAccess.checkDbConnection(out m))
            {
                WLParameters.SetAppParameters(Application.StartupPath, appParams);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Invalid server name: " + Environment.NewLine + Environment.NewLine + m, "Checking Database connection", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            buttonClose.Enabled = true;
            buttonSave.Enabled = true;
        }
    }
}
