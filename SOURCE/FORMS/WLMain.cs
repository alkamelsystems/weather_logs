﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Reflection;

using Alkamel.Utils.Mailing;
using Alkamel.Utils.Db;
using Aks.Meteo;
using Alkamel.Controls.Mailing;
using WeatherLogs.FORMS;
using WeatherLogs.REPORTS;

using Microsoft.Win32;

using com.alkameltech.utils.mailing;
using WeatherLogs.REPORTS.DATASETS;

namespace WeatherLogs
{
    public partial class WLMain : Form
    {
        // GLOBAL CLASSES

        private WLParameters AppParameters;
        private DBConnection timingDBConnection;
        private WLDataAccess WLConnection;
        private DataSet reportDataSet;
        private ReportDocument rd;

        public WLMain()
        {
            try
            {
                InitializeComponent();

                if (Properties.Settings.Default.upgradeSettings)
                {
                    Properties.Settings.Default.Upgrade();
                    Properties.Settings.Default.upgradeSettings = false;
                    Properties.Settings.Default.Save();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.StackTrace);
            }

            this.Text = "Weather Logs v." + FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion;
        }

        private void applicationPreferencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private bool updateCrystalReportsPDFRenderkeys()
        {
            try
            {
                RegistryKey regKeyAppRoot = Registry.LocalMachine.CreateSubKey(@"Software\SAP BusinessObjects\Suite XI 4.0\Crystal Reports\Export\PDF");
                regKeyAppRoot.SetValue("ForceLargerFonts", 1);

                regKeyAppRoot = Registry.LocalMachine.CreateSubKey(@"Software\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Crystal Reports\Export\Pdf");
                regKeyAppRoot.SetValue("ForceLargerFonts", 1);

                return true;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Error updating ODBC connection", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
        }

        private void WLMain_Load(object sender, System.EventArgs e)
        {
            AppParameters = WLParameters.GetAppParameters(Application.StartupPath);
            AppParameters.AdminPassword = "ESa1nI";
            WLParameters.SetAppParameters(Application.StartupPath, AppParameters);

            WLConnection = new WLDataAccess(AppParameters);

            if (WLConnection.CheckDB())
            {
                WLConnection.updateODBC();

                updateCrystalReportsPDFRenderkeys();

                timingDBConnection = new DBConnection(AppParameters.DBServer, AppParameters.DBUser, AppParameters.DBPassword, AppParameters.DBName);
            }
            else
            {
                this.Close();
            }
        }

        private bool checkDateTime()
        {
            DateTime dtFrom = timePickerFrom.Value.ToUniversalTime();
            DateTime dtTo = timePickerTo.Value.ToUniversalTime();

            int compare = DateTime.Compare(dtFrom, dtTo);
            if (compare >= 0)
            {
                MessageBox.Show("Final time must be greater than initial time.", "Time Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            try 
	        {
                if (checkDateTime())
                {

                    double secondsFrom = timePickerFrom.Value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds / 1000;
                    double secondsTo = timePickerTo.Value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds / 1000;

                    CreateDataset(AppParameters, (long)secondsFrom, (long)secondsTo);
                    CreateReport(true, "", 0);
                }
	        }
	        catch (Exception ex)
	        {
                MessageBox.Show(ex.Message, "Preview Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
	        }

        }

        private void CreateDataset(WLParameters appParameters, long start, long finish)
        {
            try 
	        {
                string SQLstr = "SELECT * FROM samples WHERE TIME_UTC_SECONDS>" + start.ToString() + " AND TIME_UTC_SECONDS<" + finish.ToString();
                Console.WriteLine(SQLstr);
                DBConnection weatherConn = new DBConnection(appParameters.DBServer, appParameters.DBUser, appParameters.DBPassword, "weather");
                DataTable data = weatherConn.getTable(SQLstr);

                reportDataSet = new Meteo();
                Meteo.MeteoRow meteoRow;

                foreach (DataRow row in data.Rows)
                {
                    meteoRow = (Meteo.MeteoRow)reportDataSet.Tables["Meteo"].NewRow();
                    meteoRow.TimeUtcSeconds = long.Parse(row["TIME_UTC_SECONDS", DataRowVersion.Current].ToString());
                    meteoRow.AirTemperature = double.Parse(row["AIR_TEMP", DataRowVersion.Current].ToString());
                    meteoRow.TrackTemperature = double.Parse(row["TRACK_TEMP", DataRowVersion.Current].ToString());
                    meteoRow.WindSpeed = double.Parse(row["WIND_SPEED", DataRowVersion.Current].ToString());
                    meteoRow.Pressure = double.Parse(row["PRESSURE", DataRowVersion.Current].ToString());
                    meteoRow.Humidity = double.Parse(row["HUMIDITY", DataRowVersion.Current].ToString());
                    meteoRow.WindDirection = (ushort)double.Parse(row["WIND_DIRECTION", DataRowVersion.Current].ToString());

                    meteoRow.ItsOK = meteoRow.ItsOK;
                    meteoRow.Rain = 0;

                    reportDataSet.Tables["Meteo"].Rows.Add(meteoRow);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error creating DataSet", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CreateReport(bool preview, string printer, int copies)
        {
            try 
	        {
                rd = new ReportDocument();
                string reportFile = Application.StartupPath + @"\REPORTS\Weather.rpt";
                if (File.Exists(reportFile))
                {
                    rd.Load(reportFile);
                    rd.SetDataSource(reportDataSet);
                    //string exportPath = Application.StartupPath + @"\EXPORTED_FILES\WeatherLogs_" + DateTime.UtcNow.ToString("yyyyMMdd_HHmmss") + ".pdf";
                    //rd.ExportToDisk(ExportFormatType.PortableDocFormat, exportPath);
                    if (preview)
                    {
                        //System.Diagnostics.Process.Start(exportPath);
                        WLReportPreview previewWindow = new WLReportPreview(rd);
                        previewWindow.ShowDialog();
                        previewWindow.Dispose();
                    }
                    else if (printer != "" && copies > 0)
                    {
                        rd.PrintOptions.PrinterName = printer;
                        rd.PrintToPrinter(copies, true, 0, 0);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error creating report", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try 
	        {
                if (checkDateTime())
                {
                    long secondsFrom = (long)timePickerFrom.Value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds / 1000;
                    long secondsTo = (long)timePickerTo.Value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds / 1000;
                    PrintResultsDialog pDialog = new PrintResultsDialog(AppParameters);
                    pDialog.ShowDialog();
                    CreateDataset(AppParameters, secondsFrom, secondsTo);
                    CreateReport(false, pDialog.getPrinter(), pDialog.getCopies());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error printing report", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCSV_Click(object sender, EventArgs e)
        {
            if (checkDateTime())
            {
                string separator = ";";
                long secondsFrom = (long)timePickerFrom.Value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds / 1000;
                long secondsTo = (long)timePickerTo.Value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds / 1000;
                StreamWriter sr;

                string folder = string.Empty;
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.FileName = "WeatherLogs_" + DateTime.UtcNow.ToString("yyyyMMdd_HHmmss") + ".csv";
                saveFileDialog1.Filter = "CSV file (*.csv)|*.csv";
                saveFileDialog1.Title = "Save CSV File";

                try
                {
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK && saveFileDialog1.FileName != "")
                    {
                        sr = new StreamWriter(saveFileDialog1.FileName, false, System.Text.Encoding.UTF8);

                        // CREATING FILE HEADER

                        string line = "";
                        line = "TIME_UTC_SECONDS" + separator + "TIME_UTC_STR" + separator + "AIR_TEMP" + separator + "TRACK_TEMP" + separator + "HUMIDITY" + separator + "PRESSURE" + separator + "WIND_SPEED" + separator + "WIND_DIRECTION" + separator;

                        Console.WriteLine(line);
                        sr.WriteLine(line);
                        sr.Flush();

                        // GETTING ROWS
                        CreateDataset(AppParameters, secondsFrom, secondsTo);

                        Console.WriteLine(reportDataSet.Tables["Meteo"].Rows);

                        foreach (DataRow row in reportDataSet.Tables["Meteo"].Rows)
                        {
                            line = "";

                            Meteo.MeteoRow meteoRow = (Meteo.MeteoRow)row;
                            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);

                            line = line + meteoRow.TimeUtcSeconds + separator;
                            line = line + origin.AddSeconds(meteoRow.TimeUtcSeconds).ToString() + separator;

                            line = line + meteoRow.AirTemperature.ToString() + separator;
                            line = line + meteoRow.TrackTemperature.ToString() + separator;
                            line = line + meteoRow.Humidity.ToString() + separator;
                            line = line + meteoRow.Pressure.ToString() + separator;
                            line = line + meteoRow.WindSpeed.ToString() + separator;
                            line = line + meteoRow.WindDirection.ToString() + separator;

                            // meteoRow.ItsOK = meteoRow.ItsOK;
                            // meteoRow.Rain = 0;

                            sr.WriteLine(line);
                            sr.Flush();
                        }

                        sr.Close();
                        MessageBox.Show("CSV created.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error creating CSV", "Error connecting to database.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void setUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicationSetupDialog aDialog = new ApplicationSetupDialog(AppParameters, timingDBConnection);
            aDialog.ShowDialog();
        }
    }
}
