﻿using System;
using System.Collections;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using Microsoft.Win32;

using Alkamel.Utils.Db;
using com.alkameltech.utils.mailing;

using WeatherLogs.FORMS;
using ReportDistributor.FORMS;

namespace WeatherLogs
{
    /// <summary>
    /// Descripción breve de WLDataAccess.
    /// </summary>
    public class WLDataAccess
    {
        private WLParameters appParams;
        private static int instances = 0;

        public WLDataAccess(WLParameters pAppParams)
        {
            conn = null;
            RDconn = null;
            appParams = pAppParams;
            WLDataAccess.instances++;
            Console.WriteLine("------------------------------- NEW WLDataAccess, total: " + WLDataAccess.instances);
        }

        ~WLDataAccess()
        {
            if (conn != null && conn.State != ConnectionState.Closed)
                conn.Close();

            if (RDconn != null && RDconn.State != ConnectionState.Closed)
                RDconn.Close();

            WLDataAccess.instances--;
            Console.WriteLine("------------------------------- DELETED WLDataAccess, total: " + WLDataAccess.instances);
        }


        public bool CheckDB()
        {

            string message = "";

            if (checkDbConnection(out message))
                return true;
            else
            {
                CheckDBConnectionsDialog cDialog = new CheckDBConnectionsDialog(appParams, this, message);
                cDialog.ShowDialog();

                if (cDialog.DialogResult == DialogResult.OK)
                {
                    return true;
                }
                else
                    return false;
            }



        }

        public bool checkDbConnection(out string message)
        {
            try
            {
                string connStr = "server=" + appParams.DBServer + ";user id=" + appParams.DBUser + "; password=" + appParams.DBPassword + "; database=" + appParams.DBName + "; pooling=false; Connect Timeout=30";
                MySqlConnection c = new MySqlConnection(connStr);
                c.Open();
                c.Close();
                message = "ok";
                return true;
            }
            catch (Exception e)
            {
                message = e.Message;
                return false;
            }
        }

        public bool updateODBC()
        {
            try
            {
                RegistryKey regKeyAppRoot = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\ODBC\ODBC.INI\ODBC Data Sources");
                regKeyAppRoot.SetValue("TK_A1GP", "MySQL ODBC 3.51 Driver");

                regKeyAppRoot = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\ODBC\ODBC.INI\TK_A1GP");
                regKeyAppRoot.SetValue("Driver", "C:\\WINDOWS\\system32\\myodbc3.dll");
                regKeyAppRoot.SetValue("DATABASE", appParams.DBName);
                regKeyAppRoot.SetValue("PWD", appParams.DBPassword);
                regKeyAppRoot.SetValue("SERVER", appParams.DBServer);
                regKeyAppRoot.SetValue("UID", appParams.DBUser);

                return true;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Error updating ODBC connection", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
        }


        # region TIMEKEEPING DATA ACCESS

        private MySqlConnection conn;
        //private DataTable data;
        private MySqlDataAdapter da;
        private MySqlCommandBuilder cb;



        public bool refreshDBConnection(WLParameters AppParams)
        {

            if (AppParams == null)
            {
                MessageBox.Show("It isn't a dbconnection defined", "Error connecting to database.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (conn != null)
                conn.Close();

            string connStr = "server=" + AppParams.DBServer + ";user id=" + AppParams.DBUser + "; password=" + AppParams.DBPassword + "; database=" + AppParams.DBName + "; pooling=false";

            try
            {
                conn = new MySqlConnection(connStr);
                conn.Open();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Cannot connect to " + AppParams.DBName + " data base.", "Error connecting to database.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Console.WriteLine(e.ToString());
                return false;
            }


        }

        public void ExecuteQuery(string SQLstr)
        {
            if (conn == null)
            {
                if (!refreshDBConnection(appParams))
                    return;
            }

            MySqlCommand cmdExec = new MySqlCommand(SQLstr, conn);
            cmdExec.ExecuteNonQuery();

        }

        #endregion

        # region REPORT DISTRIBUTOR DATA ACCESS

        private MySqlConnection RDconn;
        private MySqlDataAdapter RDda;
        private MySqlCommandBuilder RDcb;

        private bool refreshWLConnection(WLParameters AppParams)
        {

            if (AppParams == null)
            {
                MessageBox.Show("It isn't a dbconnection defined", "Error connecting to database.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (RDconn != null)
                RDconn.Close();

            string connStr = "server=" + AppParams.DBServer + ";user id=" + AppParams.DBUser + "; password=" + AppParams.DBPassword + "; database=" + AppParams.DBName + "; pooling=false";

            Console.WriteLine(connStr);

            try
            {
                RDconn = new MySqlConnection(connStr);
                RDconn.Open();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Cannot connect to Weather Logs data base.", "Error connecting to database.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Console.WriteLine(e.ToString());
                return false;
            }


        }

        public bool executeRDQuery(string SQLstr)
        {
            if (RDconn == null)
            {
                if (!refreshWLConnection(appParams))
                    return false;
            }

            try
            {
                MySqlCommand cmdExec = new MySqlCommand(SQLstr, RDconn);
                cmdExec.ExecuteNonQuery();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error executing RD Query", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public DataTable getRDTable(WLParameters AppParams, string query)
        {
            if (RDconn == null)
            {
                if (!refreshWLConnection(AppParams))
                    return null;
            }

            DataTable data = new DataTable();

            try
            {

                da = new MySqlDataAdapter(query, RDconn);
                cb = new MySqlCommandBuilder(da);
                Console.WriteLine(query);
                da.Fill(data);



                if (data.Rows.Count > 0)
                    return data;
                else
                    return null;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error getting Data Table:" + query, "Error connecting to database.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Console.WriteLine(e.ToString());
                return null;
            }

        }


        #endregion

        #region Virtual PRINTERS

        public long getMaxValue(string field, string table)
        {
            string sql = "SELECT max(" + field + ") as maxValue FROM " + table;

            RDda = new MySqlDataAdapter(sql, RDconn);
            RDcb = new MySqlCommandBuilder(RDda);
            DataTable data = new DataTable();
            RDda.Fill(data);

            long max = 0;

            if (data.Rows.Count > 0)
            {
                long.TryParse(data.Rows[0]["maxValue", DataRowVersion.Current].ToString(), out max);
            }

            return max;
        }

        #endregion
    }
}
