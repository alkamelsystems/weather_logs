﻿using System;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Windows.Forms;

using WeatherLogs.FORMS;

namespace WeatherLogs
{
    [Serializable]
    public class WLParameters
    {
        public WLParameters()
        {
            dbServer = "";
            dbPort = "";
            dbPassword = "";
            dbUser = "";
            dbName = "";
        }

        #region TimeKeeping DB Connection

        public string DBServer
        {
            get { return dbServer; }
            set { dbServer = value; }
        }
        private string dbServer;

        public string DBUser
        {
            get { return dbUser; }
            set { dbUser = value; }
        }
        private string dbUser;

        public string DBPassword
        {
            get { return dbPassword; }
            set { dbPassword = value; }
        }
        private string dbPassword;

        public string DBName
        {
            get { return dbName; }
            set { dbName = value; }
        }
        private string dbName;

        public string DBPort
        {
            get { return dbPort; }
            set { dbPort = value; }
        }
        private string dbPort;

        #endregion

        public string AdminPassword { get; set; }

        private static string charsOK = "ABCDEFGHIJKLMNOPQRSTUVWXYZÑabcdefghijklmnopqrstuvwxyzñ1234567890";
        private static string[] patron = { "m", "2", "ñ", "T", "L", "e", "Ñ", "c", "h", "V", "J", "p", "j", "L", "N", "a", "i", "J", "A", "V", "F", "f", "R", "b", "G", "2", "p", "O", "m", "u", "8", "D", "o", "7", "h", "ñ", "m", "Q", "i", "Z", "e", "x", "Y", "e", "c", "N", "R", "P", "b", "J", "W", "s", "k", "P", "G", "G", "k", "Y", "M", "U", "z", "M", "ñ", "x", "X", "F", "B", "m", "o", "X", "z", "S", "k", "m", "ñ", "T", "I", "x", "9", "4", "k", "Ñ", "w", "d", "Z", "Q", "d", "Z", "1", "a", "P", "O", "E", "O", "j", "S", "7", "k", "H", "Y", "M", "o", "l", "E", "U", "D", "H", "8", "Ñ", "x", "9", "y", "Z", "T", "t", "Ñ", "a", "X", "Z", "G", "m", "n", "w", "N", "H", "k", "O", "W", "C", "i", "X", "h", "p", "l", "s", "7", "z", "r", "J", "V", "I", "f", "0", "D", "c", "b", "z", "w", "e", "K", "2", "k", "M", "A", "w", "j", "2", "Z", "8", "u", "ñ", "v", "6", "1", "a", "b", "J", "y", "l", "x", "k", "t", "N", "E", "9", "G", "n", "s", "u", "q", "w", "b", "2", "3", "r", "w", "W", "o", "ñ", "I", "E", "Ñ", "u", "Ñ", "e", "Y", "k", "I", "n", "B", "T", "B", "K", "s", "j", "I", "Y", "H", "C", "p", "6", "K", "p", "S", "8", "Z", "4", "C", "K", "o", "F", "e", "8", "c", "s", "K", "0", "x", "k", "k", "g", "f", "F", "s", "Y", "u", "R", "J", "T", "i", "Z", "O", "l", "L", "N", "5", "v", "c", "b", "G", "d", "h", "9", "9", "6", "y" };

        private void createPatron()
        {
            string charsOK = "ABCDEFGHIJKLMNOPQRSTUVWXYZÑabcdefghijklmnopqrstuvwxyzñ1234567890";
            Random randomGenerator = new Random();

            string patronStr = "string[] patron = { ";
            for (int i = 0; i < 256; i++)
            {
                patronStr += "\"" + charsOK[randomGenerator.Next(charsOK.Length)] + "\", ";
            }

            patronStr = patronStr.Substring(0, patronStr.Length - 2) + " };";
            Console.WriteLine(patronStr);
        }

        public static bool validateString(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (!charsOK.Contains(str.Substring(i, 1)))
                    return false;
            }

            return true;
        }

        public static string encodeString(string str)
        {
            try
            {
                string encodedStr = "";
                for (int i = 0; i < str.Length; i++)
                {
                    if (charsOK.Contains(str.Substring(i, 1)))
                    {
                        int c = (charsOK.IndexOf(str[i]) + charsOK.IndexOf(patron[i][0])) % charsOK.Length;
                        encodedStr += charsOK[c];
                    }
                    else throw new Exception("Invalid string to encode, it contains invalid caracters");
                }
                return encodedStr;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error encoding string:" + ex.Message);
                throw ex;
            }
        }

        public static string decodeString(string str)
        {
            try
            {
                string decodedStr = "";
                for (int i = 0; i < str.Length; i++)
                {
                    if (charsOK.Contains(str.Substring(i, 1)))
                    {
                        int c = charsOK.IndexOf(str[i]) - charsOK.IndexOf(patron[i][0]);
                        if (c <= 0) c += charsOK.Length;
                        decodedStr += charsOK[c];
                    }
                    else throw new Exception("Invalid string to decode, it contains invalid caracters");
                }
                return decodedStr;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error encoding string:" + ex.Message);
                throw ex;
            }
        }

        public static WLParameters GetAppParameters(string appPath)
        {
            XmlSerializer x = new XmlSerializer(typeof(WLParameters));

            // Read the XML file if it exists ---
            FileStream fs = null;
            try
            {

                fs = new FileStream(appPath + @"\WLParameters.xml", FileMode.Open);

                // Deserialize the content of the XML file to a Contact array 
                // utilizing XMLReader
                System.Xml.XmlReader reader = new System.Xml.XmlTextReader(fs);
                WLParameters appParams = (WLParameters)x.Deserialize(reader);
                return appParams;
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
                // Do nothing if the file does not exists
            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }

        public static void SetAppParameters(string appPath)
        {
            WLParameters appParams = new WLParameters();

            appParams.DBPort = "3306";
            appParams.DBServer = "192.168.74.11";
            appParams.DBUser = "timekeeper";
            appParams.DBPassword = "timekeeper";
            appParams.DBName = "TimeKeeper_A1GP";

            appParams.AdminPassword = "ESa1nI";

            SetAppParameters(appPath, appParams);
        }

        public static void SetAppParameters(string appPath, WLParameters appParams)
        {
            Stream stm = new FileStream(appPath + @"\WLParameters.xml", FileMode.Create);
            XmlSerializer x = new XmlSerializer(typeof(WLParameters));
            x.Serialize(stm, appParams);
            stm.Flush();
            stm.Close();
        }
    }
}
